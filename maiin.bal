import ballerina/io;
import ballerina/graphql;

public type CovidEntries record {|
    readonly string regionCode;
    string region;
    int cases = 0;
    int deaths = 0;
    int recovered = 0;
    int active = 0;
    string date= "20/08/2022";
|};

table<CovidEntries> key(regionCode) covidEntriesTable = table [
        {regionCode: "KH", region: "Khomas", cases: 0,date: "20/08/2022", deaths: 0, recovered: 0, active: 0},
        {regionCode: "OS", region: "Oshana", cases: 0, date: "20/08/2022",deaths: 0, recovered: 0, active: 0},
        {regionCode: "OM", region: "Omusati", cases: 0,date: "20/08/2022" ,deaths: 0, recovered: 0, active: 0},
        {regionCode: "HA", region: "Hardap", cases: 0,date: "20/08/2022", deaths: 0, recovered: 0, active: 0},
        {regionCode: "OE", region: "Okavango East", cases: 0,date: "20/08/2022", deaths: 0, recovered: 0, active: 0},
        {regionCode: "OW", region: "Okavango West", cases: 0,date: "20/08/2022", deaths: 0, recovered: 0, active: 0},
        {regionCode: "ER", region: "Erongo", cases: 0,date: "20/08/2022", deaths: 0, recovered: 0, active: 0},
        {regionCode: "OT", region: "Otjozondjupa", cases: 0,date: "20/08/2022", deaths: 0, recovered: 0, active: 0},
        {regionCode: "ZA", region: "Zambezi", cases: 0,date: "20/08/2022", deaths: 0, recovered: 0, active: 0}
    ];

public distinct service class CovidData {
    private final readonly & CovidEntries entryRecord;

    function init(CovidEntries entryRecord) {
        self.entryRecord = entryRecord.cloneReadOnly();
    }

    resource function get regionCode() returns string {
        return self.entryRecord.regionCode;
    }

    resource function get region() returns string {
        return self.entryRecord.region;
    }
  resource function get date() returns string {
        return self.entryRecord.date;
    }

    resource function get cases() returns int? {
        return self.entryRecord.cases;
    }

    resource function get deaths() returns int? {
        return self.entryRecord.deaths;
    }

    resource function get recovered() returns int? {
        return self.entryRecord.recovered;
    }

    resource function get active() returns int? {
        return self.entryRecord.active;
    }
}

public type Entry record {|
    int deaths = 0;
    int recovered = 0;
    int new_case = 0;
    string date ="";
|};

service /graphql on new graphql:Listener(4000) {
    resource function get all() returns CovidData[] {
        CovidEntries[] covidEntries = covidEntriesTable.toArray().cloneReadOnly();
        return covidEntries.map(entry => new CovidData(entry));
    }

    resource function get filter(string regionCode) returns CovidData? {
         CovidEntries? covidEntry = covidEntriesTable[regionCode];
        if covidEntry is CovidEntries {
            return new (covidEntry);
        }
        return;
    }

    remote function update(string regionCode, Entry entry) returns CovidData {
        CovidEntries e = covidEntriesTable.get(regionCode);
        io:println(e.toBalString());

        if (entry.new_case > 0) {
            e.cases += entry.new_case;
            e.active += entry.new_case;
        }

        if (entry.deaths > 0) {
            e.deaths += entry.deaths;
            e.active -= entry.deaths;
        }

        if (entry.recovered > 0) {
            e.recovered += entry.recovered;
            e.active -= entry.recovered;
        }

        // covidEntriesTable.put(entry);
        return new CovidData(e);
    }
}
