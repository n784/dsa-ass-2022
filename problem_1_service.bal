import ballerina/grpc;
import ballerina/log;

listener grpc:Listener ep = new (9090);



map<UserRequest> users = {
    "clive": {profile: ADMINISTRATOR, password: "1234"},
    "meke": {profile: ASSESSOR, password: "1234"},
    "Naomi": {profile: LEARNER, password: "1234"}
};

map<Course> courses = {
    "MIT": {
        code: "MIT",
        name: "Mathemetics for IT",
        assessor: "assessor",
        students: [],
        assignments: [
            {
                id: 200,
                weight: 0.6
            }
        ]
    }
};

table<Submission> submissions = table [
        {
            courseCode: "dsp",
            assignmentID: 200,
            studentID: "learner",
            content: "This the assignment data",
            mark: 0.0,
            marked: false
        },
        {
            courseCode: "dsp",
            assignmentID: 200,
            studentID: "learner2",
            content: "This the learner2 assignment data",
            mark: 0.0,
            marked: false
        }
    ];


@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR_PROBLEM1, descMap: getDescriptorMapProblem1()}
service "problem_1" on ep {

      remote function create_course(Problem_1StringCaller caller,stream<CourseRequest, grpc:Error?> clientStream) returns error? {
 check clientStream.forEach(function(CourseRequest value) {
    
            log:printInfo("Received:", value = value);
            courses[value.courseCode] = {code: value.courseCode, name: value.name, assignments: value.assignments};

            log:printInfo(courses.toBalString());
    checkpanic caller->sendString(value.courseCode);
            log:printInfo("Sent:", value = value.courseCode);

        });

        log:printInfo("Closing stream");
        check caller->complete();
          
      }

    remote function assign_course(AssignRequest value) returns boolean|error {
   // TODO: Check if user initiating request is an administrator and also if the course exists
        courses[value.courseCode]["assessor"] = value.assessorCode;
        return true;}


    remote function submit_mark(Mark value) returns boolean|error {
        // assessor submits the marks for assignments
        log:printInfo("Checking....");
        foreach Submission submission in submissions {
            if (submission.assignmentID == value.assignmentID && submission.courseCode == value.courseID && submission.studentID == value.studentID && !submission.marked) {
                submission.mark = value.mark;
                submission.marked = true;
            }
        }

        log:printInfo(submissions.toBalString());

        return true;
    } 
    
    remote function login(LoginRequest value) returns LoginResponse|error {
         if (users.hasKey(value.userID)) {
            UserRequest? user = users[value.userID];

            if (user is UserRequest && user.password == value.password) {
                Profiles? profile = user["profile"];
                if (profile is Profiles) {
                    return {success: true, profile: profile};
                }
            }

        }
        return {success: false};
    }


    remote function create_user(stream<UserRequest, grpc:Error?> clientStream) returns CreateUserResponse|error {
          CreateUserData[] result = [];

        check clientStream.forEach(function(UserRequest value) {
            if (users.hasKey(value.userCode)) {
                // user already exists
                result.push({userCode: value.userCode, status: "Failed, already exists"});
            } else {
                value.password = "password";
                users[value.userCode] = value;
                result.push({userCode: value.userCode, status: "Created"});
            }
        });

        return {data: result};
    }
    remote function submit_assignment(stream<AssignmentRequest, grpc:Error?> clientStream) returns boolean|error {
       
        check clientStream.forEach(function(AssignmentRequest value) {
            submissions.add({studentID: value.studentID, content: value.content, assignmentID: value.assignmentID, courseCode: value.courseCode});
        });

        log:printInfo(submissions.toBalString());

        return true; 
    }
    remote function register(stream<Register, grpc:Error?> clientStream) returns boolean|error {
        check clientStream.forEach(function(Register value) {
            log:printInfo("Received:", value = value);

            string[]? ids = courses[value.courseID]["students"];
            if (ids is string[]) {
                ids.push(value.userID);
                courses[value.courseID]["students"] = ids;
            }
        });

        log:printInfo(courses.toJsonString());

        return true; 
    }
    remote function request_assignment(AssessorRequest value) returns stream<Submission, error?>|error {
        Submission[] res = [];

        // Check if user is assessor
        if (courses[value.courseCode]["assessor"] == value.assessorCode) {
            log:printInfo("IS assessorCode ");

            foreach Submission submission in submissions {
                if (submission.assignmentID == value.assignmentID && submission.courseCode == value.courseCode && !submission.marked) {
                    res.push(submission);
                }
            }

        }

        return res.toStream(); 
    }
   
    }


