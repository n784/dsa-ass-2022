import ballerina/http;
import ballerina/io;

public function main() returns error? {
    // Creates a new client with the backend URL.
    final http:Client clientEndpoint = check new ("http://localhost:4000");

    check LoginUI(clientEndpoint);
}

function LoginUI(http:Client clientEndpoint) returns error? {
    io:println("            "+"COVID19 QUERIES"+"            ");
    io:println("-----------------------------------------");
    io:println("select an option?");
    io:println("1. Get  all covid statatistics");
    io:println("2. request covid starts by region ");
    io:println("3. Update  covid statstistics ");
    io:println("-----------------------------------------");
    string choice = io:readln("Enter option ");

    json resp;

    if (choice == "1") {
        resp = check clientEndpoint->post("/graphql", {query: "{ all { region date cases active } }"});
        io:println(resp.toJsonString());
    } else if (choice == "2") {
        string regionCode = io:readln("Enter the region code to request by filter: ");
        resp = check clientEndpoint->post("/graphql", {query: "{ filter(regionCode: \"" + regionCode + "\" ) { region cases date } }"});
        io:println(resp.toJsonString());

    } else if (choice == "3") {
        string iso = io:readln("Enter region code: ");
        string cases = io:readln("Enter new cases: ");
        string deaths = io:readln("Enter deaths: ");
        string recovery = io:readln("Enter recoveries: ");
        string date = io:readln ("enter the date");

        resp = check clientEndpoint->post("/graphql", {query: "mutation{ update(regionCode: \""+iso+"\", entry: {deaths: "+deaths+", recovered: "+recovery+", new_case: "+cases+"date: "+date+"}) { region cases recovered deaths active } }"});
        io:println(resp.toJsonString());
    }


    check LoginUI(clientEndpoint);

}
